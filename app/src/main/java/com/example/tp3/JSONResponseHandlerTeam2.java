package com.example.tp3;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import com.example.tp3.Team;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam2 {

    private static final String TAG = JSONResponseHandlerTeam2.class.getSimpleName();

    /**
     * The team
     */
    private Team team;

    /**
     * Constructor
     * @param team
     */
    public JSONResponseHandlerTeam2(Team team) {
        this.team = team;
    }


    public void readJsonStreamRank(InputStream response) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readRank(reader);
        } finally {
            reader.close();
        }
    }

    /**
     * Read the events
     * @param reader
     * @throws IOException
     */

    public void readRank(JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("table")) {
                readArrayRank(reader);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    /**
     * Read the rank
     * @param reader
     * @throws IOException
     */
    private void readArrayRank(JsonReader reader) throws IOException {

        reader.beginArray();
        int nb = 0; // current index
        while (reader.hasNext() ) {

            reader.beginObject();
            nb++;
            while (reader.hasNext()) {
                // Get the field name
                String name = reader.nextName();
                // If the field is the name of the team
                if (name != null && name.equals("name")) {
                    String teamName = reader.nextString();
                    String currentTeamName = team.getName();
                    if (teamName != null && teamName.equals(currentTeamName)) {
                        team.setRanking(nb);
                    }
                    while (reader.hasNext()) {
                        String nameBis = reader.nextName();
                        if (nameBis.equals("total")) {
                            team.setTotalPoints(reader.nextInt());
                            break;
                        } else {
                            reader.skipValue();
                        }
                    }
                } else {
                    if (reader.hasNext()) {
                        reader.skipValue();
                    }
                }
            }
            reader.endObject();
        }
        reader.endArray();
    }

}
