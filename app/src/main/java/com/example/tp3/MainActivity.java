package com.example.tp3;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    public static TeamAdapter adapter;
    public static SportDbHelper dbhelper;
    public static SportDbHelper dbHelper;
    public static int nbrThreadsRefresh = 0;
    RecyclerView recyclerView;
    ListView mliste;
    public static SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mliste= findViewById(R.id.mliste);
        this.recyclerView = findViewById(R.id.RecyclerView);
        swipe = findViewById(R.id.swipe);
        swipe.setOnRefreshListener(this);

        new ItemTouchHelper(removeCallback).attachToRecyclerView(recyclerView);
       /* adapter = new SimpleCursorAdapter(
           this,
           android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams(), new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME },
           new int[] { android.R.id.text1, android.R.id.text2}
           );*/
        adapter = new TeamAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemViewCacheSize(0);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

       /* mliste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Team team = dbhelper.getAllTeams().get(position);

                Intent mIntent = new Intent(MainActivity.this, TeamActivity.class);
                mIntent.putExtra(Team.TAG, team);
                MainActivity.this.startActivity(mIntent);
            }
        });*/

        FloatingActionButton fab = findViewById(R.id.fab);
        dbhelper = new SportDbHelper(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                MainActivity.this.startActivityForResult(mIntent,1);
            }
        });

       // dbHelper = new SportDbHelper(this);
//        dbHelper.populate();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {

            Team team = (Team) data.getParcelableExtra(Team.TAG);

            if (team != null) {
                MainActivity.dbhelper.addTeam(team);
                Toast.makeText(this, "Team saved !", Toast.LENGTH_SHORT);
            } else {
                Toast.makeText(this, "The team is strange !", Toast.LENGTH_SHORT);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        for (Team team : MainActivity.dbhelper.getAllTeams()) {

            RefreshTeam running = new RefreshTeam();
            running.execute(team);

            //MainActivity.nbrThreadsRefresh++;
        }

    }



    ItemTouchHelper.SimpleCallback removeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            int position = viewHolder.getAdapterPosition();
            long id = MainActivity.dbhelper.getAllTeams().get(position).getId();

            MainActivity.dbhelper.deleteTeam(id);

            adapter.notifyDataSetChanged();
        }
    };

    private final class RefreshTeam extends AsyncTask<Team, Void, String> {

        @Override
        protected String doInBackground(Team... params) {

            Team team = params[0];



            JSONResponseHandlerTeam responseHandlerTeam = new JSONResponseHandlerTeam(team);
            JSONResponseHandlerTeam1 responseHandlerTeam1 = new JSONResponseHandlerTeam1(team);
            JSONResponseHandlerTeam2 responseHandlerTeam2 = new JSONResponseHandlerTeam2(team);

            // Update image
            TeamActivity.loadTeamContent(team, responseHandlerTeam, MainActivity.this);
            TeamActivity.loadTeamLastEvent(team, responseHandlerTeam1, MainActivity.this);
            TeamActivity.loadTeamRank(team, responseHandlerTeam2, MainActivity.this);

            MainActivity.dbhelper.updateTeam(team);

            return TeamActivity.VALIDE;
        }

        @Override
        protected void onPostExecute(String result) {

            MainActivity.nbrThreadsRefresh--;
            if (MainActivity.nbrThreadsRefresh <= 0) {

                // TODO : Fix doesn't work
                MainActivity.adapter.notifyDataSetChanged();
                MainActivity.swipe.setRefreshing(false);
                Log.d("RefreshTeamContent","Finished");
            }
        }
    }
}
