package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.ViewHolder> {

    private Context context;

    public TeamAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        Team team = MainActivity.dbhelper.getAllTeams().get(position);

        Picasso.with(context).load(team.getTeamBadge()).placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(holder.image, new com.squareup.picasso.Callback(
                ) {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError(){

                    }
                });

        holder.teamName.setText(team.getName());
        holder.leagueName.setText(team.getLeague());
        holder.lastMatch.setText(team.getLastEvent().toString());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(context, TeamActivity.class);
                mIntent.putExtra(Team.TAG, MainActivity.dbhelper.getAllTeams().get(position));
                context.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return MainActivity.dbhelper.getAllTeams().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout parentLayout;

        ImageView image;
        TextView teamName;
        TextView leagueName;
        TextView lastMatch;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            parentLayout = itemView.findViewById(R.id.row);

            image = itemView.findViewById(R.id.image);
            teamName = itemView.findViewById(R.id.teamName);
            leagueName = itemView.findViewById(R.id.leagueName);
            lastMatch = itemView.findViewById(R.id.lastMatch);
        }
    }
}
