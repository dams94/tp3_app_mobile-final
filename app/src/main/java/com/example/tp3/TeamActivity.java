package com.example.tp3;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.squareup.picasso.Picasso;
import java.io.InputStream;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;


public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;
    private ImageView imageBadge;

    private JSONResponseHandlerTeam responseHandlerTeam;
    private JSONResponseHandlerTeam1 responseHandlerTeam1;
    private JSONResponseHandlerTeam2 responseHandlerTeam2;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;
    public static Context c;
    private Team team;

    public static String VALIDE = "Executed";
    public static String ECHEC = "Failed";;
    private FetchTeam  runningASY ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);
        responseHandlerTeam1 = new JSONResponseHandlerTeam1(team);
        responseHandlerTeam = new JSONResponseHandlerTeam(team);
        responseHandlerTeam2 = new JSONResponseHandlerTeam2(team);

        imageBadge = (ImageView) findViewById(R.id.imageView);
        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        updateView();

        final Button update = (Button) findViewById(R.id.button);

        update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (runningASY != null)
                    runningASY.cancel(true);

                runningASY = new FetchTeam();
                runningASY.execute();

            }
        });

    }

    @Override
    public void onBackPressed() {

        // TODO : prepare result for the main activity
        MainActivity.dbhelper.updateTeam(team);

        MainActivity.adapter.notifyDataSetChanged();

        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        Picasso.with(this).load(team.getTeamBadge()).placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(imageBadge, new com.squareup.picasso.Callback(
                ) {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError(){

                    }
                });

}
 public static String loadTeamContent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

       HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {

//            Verify if we are currently connected
            if (activeNetwork != null) {
                URL url = WebServiceUrl.buildSearchTeam(team.getName());
                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStream(response);

                } else {
                    Toast.makeText(c,"Serveurs indisponibles,Veuillez réessayer plus tard !",Toast.LENGTH_SHORT).show();
                }

            }

        } catch (Exception e) {
            return TeamActivity.ECHEC;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.ECHEC;
        }

    }

    public static String loadTeamLastEvent(Team team, JSONResponseHandlerTeam1 responseHandlerTeam1, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {

//            Verify if we are currently connected
            if (activeNetwork != null) {

                /**
                 * Fetch the last event
                 */
                URL url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection != null && urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam1.readJsonStreamLastEvent(response);

                } else {
                    Toast.makeText(c,"Serveurs indisponibles,Veuillez réessayer plus tard !",Toast.LENGTH_SHORT).show();

                }


            }

        } catch (Exception e) {
            return TeamActivity.ECHEC;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.VALIDE;
        }

    }

    public static String loadTeamRank(Team team, JSONResponseHandlerTeam2 responseHandlerTeam2, Context context) {

        HttpsURLConnection urlConnection = null;
        try {
            /**
                 * Fetch the last event
                 */
                URL url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam2.readJsonStreamRank(response);

                } else {
                    Toast.makeText(c,"Serveurs indisponibles,Veuillez réessayer plus tard !",Toast.LENGTH_SHORT).show();
                }

        } catch (Exception e) {
            return TeamActivity.ECHEC;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.VALIDE;
        }

    }

    private final class FetchTeam extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            String req1 = loadTeamContent(team, responseHandlerTeam, TeamActivity.this);
            String req2 = loadTeamLastEvent(team, responseHandlerTeam1, TeamActivity.this);
            String req3 = loadTeamRank(team, responseHandlerTeam2, TeamActivity.this);

            if (req1.equals(TeamActivity.VALIDE) && req2.equals(TeamActivity.VALIDE) && req3.equals(TeamActivity.VALIDE)) {
                return TeamActivity.VALIDE;
            } else {
                return TeamActivity.ECHEC;
            }
        }
        @Override
        protected void onPostExecute(String result) {

            updateView();

            Log.d("BETA12599","View update !");
        }
    }
}
