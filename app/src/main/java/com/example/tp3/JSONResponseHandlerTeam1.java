package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import com.example.tp3.Team;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam1 {

    private static final String TAG = JSONResponseHandlerTeam1.class.getSimpleName();

    /**
     * The team
     */
    private Team team;

    /**
     * Constructor
     * @param team
     */
    public JSONResponseHandlerTeam1(Team team) {
        this.team = team;
    }


    /**
     * Parse to JSON the response of the last event API
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStreamLastEvent(InputStream response) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readLastEvent(reader);
        } finally {
            reader.close();
        }
    }
    /**
     * Read the events
     * @param reader
     * @throws IOException
     */
    public void readLastEvent(JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("results")) {
                readArrayLastEvent(reader);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    /**
     * Read the first last event
     * @param reader
     * @throws IOException
     */
    private void readArrayLastEvent(JsonReader reader) throws IOException {
        boolean foundEventWithNonNullScores = false;
        /**
         * The output match
         */
        Match match = new Match(-1, "", "", "", 0, 0);
        reader.beginArray();

        int nb = 0; // only consider the first element of the array

        // For each last event
        while (reader.hasNext() ) {

            reader.beginObject();
            // For each data
            while (reader.hasNext()) {
                // Data label
                String name = reader.nextName();
                if (nb==0) {
                    // Get the informations below
                    if (!foundEventWithNonNullScores) {
                    if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore") && reader.peek() != JsonToken.NULL) {
                        match.setHomeScore(reader.nextInt());
                    }  else if (name.equals("intAwayScore") && reader.peek() != JsonToken.NULL) {
                        match.setAwayScore(reader.nextInt());
                        foundEventWithNonNullScores = true;
                    } else {
                        reader.skipValue();
                    }

                }
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
        team.setLastEvent(match);
    }

}
